#include <Arduino.h>
#line 1 "/home/awgrover/dev/personal/copypasta/modules/examples/blink_faster/blink_faster.ino"
/* blink.ino */

// includes
// globals

constexpr unsigned int blinker = 5;
constexpr unsigned long blink_interval = 100;

#line 9 "/home/awgrover/dev/personal/copypasta/modules/examples/blink_faster/blink_faster.ino"
void setup();
#line 21 "/home/awgrover/dev/personal/copypasta/modules/examples/blink_faster/blink_faster.ino"
void id();
#line 26 "/home/awgrover/dev/personal/copypasta/modules/examples/blink_faster/blink_faster.ino"
void loop();
#line 9 "/home/awgrover/dev/personal/copypasta/modules/examples/blink_faster/blink_faster.ino"
void setup() {
  // FIXME: a module, which needs ordering "1st"  (singleton/resource)
  id();
  
  // FIXME: this could be a module: (singleton/resource)
  pinMode(blinker,OUTPUT); // FIXME: should be a declaration of what we need

  digitalWrite(blinker, LOW);
  Serial.print("Blink pin "); Serial.print(blinker); Serial.print(" at interval "); Serial.println(blink_interval);
  }

// FIXME: a module, a default one
void id() {
  Serial.print(F( "Start " __FILE__ " " __DATE__ " " __TIME__ " gcc " __VERSION__ " ide "));
  Serial.println(ARDUINO);
}

void loop() {
  // FIXME: the "every" should be a module -> event
  static unsigned long blink_last = millis() - blink_interval; // turn on immediately
  if ( (millis() - blink_last >= blink_interval) ) {
    digitalWrite( blinker, ! digitalRead(blinker) ); // FIXME: could be a module
    blink_last = millis();
    }
  }

/* END blink.ino */


