namespace blink {
#include "preprocessed/blink.ino.h"
};
namespace blink_faster {
#include "preprocessed/blink_faster.ino.h"
};

void setup() {
  blink::setup();
  blink_faster::setup();
}

void loop() {
  static unsigned long start_time = millis();
  static unsigned long report_time = millis();
  blink::loop();
  blink_faster::loop();

  if (millis() - report_time >= 1000) {
    report_time = millis();
    Serial.println(millis() - start_time);
  }

}
